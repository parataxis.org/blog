+++
title = "New post today"
author = ["Norman Yamada"]
draft = false
+++

Get your fresh **super** fresh super _tasty_ post here -- I'm `so very` excited. Yes, I am.

Aren't you? Don't you want to see my +beautiful\* writing?

<a id="figure--my-image"></a>

{{< figure src="/fire.png" caption="<span class=\"figure-number\">Figure 1: </span>beautiful_writing" >}}
