+++
author = ["Norman Yamada"]
draft = false
+++

## Homepage {#_index}

Test


## Blog Posts {#blog-posts}


### My Blog Homepage {#_index}

Wow, look at all my blog post!


### Trying a post {#test-post}

Why do I bother?


### New post today {#new-post-today}

Get your fresh **super** fresh super _tasty_ post here -- I'm `so very` excited. Yes, I am.

NOW! Aren't you? Don't you want to see my +beautiful\* writing?

{{< figure src="/images/fire.jpg" >}}


### Another post today {#another post today}

Get your fresh **super** fresh super _tasty_ post here -- I'm `so very` excited. Yes, I am.
